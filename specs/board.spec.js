const path = require('path');

const puppeteer = require('puppeteer');
const { expect } = require('chai');

const {
  CUSTOM_CLASSES,
  SELECTORS,
} = require('../src/_board-enhancement.class');
const TIMEOUT = 120000;
const extensionPath = path.join(process.cwd(), 'dist');
const opts = {
  args: [
    '--disable-dev-shm-usage', // Needed for CI
    `--disable-extensions-except=${extensionPath}`,
    '--disable-gbu',
    '--no-sandbox',
    `--load-extension=${extensionPath}`,
  ],
  headless: 'new',
  slowMo: 100,
  timeout: TIMEOUT,
};

describe('GitLab Details Sidebar extension', function() {
  this.timeout(TIMEOUT);
  let browser, page;

  before(async function() {
    browser = await puppeteer.launch(opts);
  });

  after(function() {
    browser.close();
  });

  beforeEach(async function() {
    page = await browser.newPage();
    page.setViewport({
      height: 768,
      width: 1024,
    });
    await page.goto('https://gitlab.com/potato-oss/gitlab-extensions/ci-test-project/-/boards');
    await page.waitForSelector(`${SELECTORS.BOARD} ${SELECTORS.CARD}`, { visible: true });
  });

  afterEach(async function() {
    // eslint-disable-next-line no-undef
    await page.evaluate(() => window.localStorage.clear());
    await page.close();
  })

  it('injects items into the sidebar', async function() {
    expect(await page.$(SELECTORS.IFRAME)).to.be.null;
    expect(await page.$(`.${CUSTOM_CLASSES.TOGGLE}`)).to.be.null;

    await page.click(`${SELECTORS.BOARD} ${SELECTORS.CARD}`);

    expect(await page.$(SELECTORS.IFRAME)).not.to.be.null;
    expect(await page.$(`.${CUSTOM_CLASSES.TOGGLE}`)).not.to.be.null;
  });

  it('only injects one iframe', async function() {
    await page.click(`${SELECTORS.BOARD} ${SELECTORS.CARD}`);

    await page.waitForSelector(SELECTORS.IFRAME, { visible: true });

    const sidebarIframes = await page.$$(SELECTORS.IFRAME);
    expect(sidebarIframes.length).to.equal(1);
  });

  it('opens the sidebar by default', async function() {
    await page.click(`${SELECTORS.BOARD} ${SELECTORS.CARD}`);

    const bodyHasClass = await page
      .$eval('body', (body, CUSTOM_CLASSES) => body.classList.contains(CUSTOM_CLASSES.COLLAPSED_STATE), CUSTOM_CLASSES);
    expect(bodyHasClass).to.be.false;

    const sidebarIframe = await page.$(SELECTORS.IFRAME);
    expect(await sidebarIframe.isIntersectingViewport()).to.be.true;
  });

  it('closes the sidebar when icon is pressed', async function() {
    await page.click(`${SELECTORS.BOARD} ${SELECTORS.CARD}`);

    await page.click(`.${CUSTOM_CLASSES.TOGGLE}`);

    const bodyHasClass = await page
      .$eval('body', (body, CUSTOM_CLASSES) => body.classList.contains(CUSTOM_CLASSES.COLLAPSED_STATE), CUSTOM_CLASSES);
    expect(bodyHasClass).to.be.true;

    const sidebarIframe = await page.$(SELECTORS.IFRAME);
    expect(await sidebarIframe.isIntersectingViewport()).to.be.false;
  });

  it('remembers the closed state', async function() {
    await page.click(`${SELECTORS.BOARD} ${SELECTORS.CARD}`);

    await page.click(`.${CUSTOM_CLASSES.TOGGLE}`);

    await page.click(`${SELECTORS.BOARD} ${SELECTORS.CARD}:last-of-type`);

    const sidebarIframe = await page.$(SELECTORS.IFRAME);
    expect(await sidebarIframe.isIntersectingViewport()).to.be.false;
  });

  const viewportWidths = [
    1200,
    1400,
    2000
  ];

  viewportWidths.forEach((viewportWidth) => {
    it(`should allow all boards to be reachable via scroll at ${viewportWidth}px`, async function() {
      page.setViewport({
        height: 800,
        width: viewportWidth,
      });

      await page.click(`${SELECTORS.BOARD} ${SELECTORS.CARD}`);

      await page.waitForSelector(SELECTORS.IFRAME, { visible: true });

      await page.$eval(SELECTORS.BOARD, boardlist => boardlist.scrollBy(boardlist.scrollWidth, 0));

      const lastColumn = await page.$('.boards-list *:nth-last-child(2)');
      const lastColumnSizing = await lastColumn.boundingBox();
      const sidebar = await page.$(`.${CUSTOM_CLASSES.SIDEBAR}`);
      const sidebarSizing = await sidebar.boundingBox();

      expect(lastColumnSizing.x + lastColumnSizing.width).to.be.lessThan(sidebarSizing.x);
    });
  });
});
