const NAMESPACE = 'gds';

const CLASSES = {
  CARD: 'board-card',
  CARD_ACTIVE: 'is-active',
  CARD_GHOST: 'is-ghost',
};

const CUSTOM_CLASSES = {
  COLLAPSED_STATE: `${NAMESPACE}--collapsed`,
  IFRAME: `${NAMESPACE}-iframe`,
  IFRAME_LOADING: `${NAMESPACE}-iframe--loading`,
  SIDEBAR: `${NAMESPACE}-sidebar`,
  SIDEBAR_HEADER: `${NAMESPACE}-sidebar-header`,
  SPINNER: `${NAMESPACE}-loading-spinner`,
  TOGGLE: `${NAMESPACE}-expanded-toggle`,
  TOGGLE_COLLAPSED: `${NAMESPACE}-expanded-toggle--collapsed`,
  TOGGLE_STYLES: 'btn btn-default btn-sm gl-button btn-default-tertiary btn-icon',
};

const SELECTORS = {
  BOARD: '.boards-list',
  CARD: `.${CLASSES.CARD}`,
  IFRAME: `.${CUSTOM_CLASSES.IFRAME}`,
  LINK: '.board-card-title a',
  NEW_ISSUE_BUTTON: '.issue-count-badge-add-button',
  NUMBER: '.board-card-number',
  SIDEBAR: '.boards-sidebar',
  SIDEBAR_HEADER: '.gl-drawer-header',
};

const LOCAL_STORAGE_KEY = 'gitlab-sidebar-expanded-state';

const debounce = (f, t) => {
  let timeout;
  return (...args) => {
    clearTimeout(timeout);
    timeout = setTimeout(() => f(...args), t);
  };
};

class BoardEnhancement {
  constructor(element) {
    if (!element) {
      return;
    }

    this.element = element;
    this.sidebar = null;
    this.sidebarHeader = null;
    this.frame = null;
    this.toggle = null;
    this.cardList = {};
    this.observer = null;

    this.clickHandler = this.clickHandler.bind(this);
    this.getCardLink = this.getCardLink.bind(this);
    this.getCardNumber = this.getCardNumber.bind(this);
    this.onBoardMutation = this.onBoardMutation.bind(this);
    this.setIssue = this.setIssue.bind(this);
    this.setUIState = this.setUIState.bind(this);
    this.styleFrame = this.styleFrame.bind(this);
    this.toggleExpansion = this.toggleExpansion.bind(this);
    this.setSidebarClasses = this.setSidebarClasses.bind(this)

    this.setIssueDebounced = debounce(this.setIssue, 1);
    this.expanded = localStorage.getItem(LOCAL_STORAGE_KEY) !== 'false';

    this.bindEvents();
    console.debug('GitLab Details Sidebar Plugin initialised...');
  }

  bindEvents() {
    this.element.addEventListener('click', this.clickHandler);
  }

  unbindEvents() {
    this.element.removeEventListener('click', this.clickHandler);
    this.observer.disconnect();
  }

  clickHandler(event) {
    if (event.target.closest(SELECTORS.NEW_ISSUE_BUTTON)) {
      if (!this.observer) {
        this.watchForNewCards();
      }
      return;
    }

    const card = event.target.closest(SELECTORS.CARD);
    if (!card || event.target.closest(SELECTORS.LINK)) {
      return;
    }

    const link = this.getCardLink(card);
    if (link) {
      this.setIssue(link);
    }
  }

  getSidebar() {
    const sidebars = document.querySelectorAll(SELECTORS.SIDEBAR);
    return sidebars[sidebars.length - 1];
  }

  setSidebarClasses() {
    if (this.sidebar) {
      this.sidebar.classList.add(CUSTOM_CLASSES.SIDEBAR);
    }
  }

  rebuildUI() {
    this.sidebar = this.getSidebar();
    this.sidebarHeader = this.sidebar.querySelector(SELECTORS.SIDEBAR_HEADER);
    this.sidebar.classList.add(CUSTOM_CLASSES.SIDEBAR);

    /*
     * GitLab seems to be resetting custom classes on the sidebar after the animate-in
     * transition (or at some point anyway after the element is first added to the dom).
     * We add it here to ensure it's added back again if it wasn't
     */
    this.sidebar.addEventListener('transitionend', this.setSidebarClasses);

    this.sidebarHeader.classList.add(CUSTOM_CLASSES.SIDEBAR_HEADER);
    if (this.toggle) {
      this.toggle.parentElement.removeChild(this.toggle);
      this.toggle = null;
    }
    const iframe = document.querySelector(SELECTORS.IFRAME);
    if (iframe) {
      iframe.parentElement.removeChild(iframe);
    }
    this.frame = null;
    this.createFrame();
  }

  getCardLink(card) {
    const link = card.querySelector(SELECTORS.LINK);
    if (!link) {
      console.warn('No link found on the current card.');
      return;
    }
    return link.href;
  }

  getCardNumber(card) {
    const numberEl = card.querySelector(SELECTORS.NUMBER);
    if (!numberEl) {
      console.warn('No number found on the current card.');
      return;
    }
    return numberEl.textContent.trim().replace('#', '');
  }

  setIssue(url) {
    console.log(`Setting iframe URL to: ${url}`);
    this.rebuildUI();
    this.frame.classList.add(CUSTOM_CLASSES.IFRAME_LOADING);
    this.frame.src = url;
  }

  createFrame() {
    this.frame = document.createElement('iframe');

    if (document.querySelector(SELECTORS.IFRAME)) {
      // We have somehow double initialised, don't create a second frame, die gracefully
      console.warn('GitLab Details Sidebar appears to have been initialised twice');
      this.destroy();
      return;
    }

    this.toggle = document.createElement('button');
    this.frame.addEventListener('load', this.styleFrame);
    this.toggle.addEventListener('click', this.toggleExpansion);

    const div = document.createElement('div');
    const spinner = document.createElement('span');
    const sidebarContent = this.sidebar.firstElementChild;

    div.appendChild(spinner);
    div.appendChild(this.toggle);
    div.appendChild(this.frame);
    div.className = CUSTOM_CLASSES.IFRAME;
    spinner.className = CUSTOM_CLASSES.SPINNER;
    this.toggle.className = `${CUSTOM_CLASSES.TOGGLE} ${CUSTOM_CLASSES.TOGGLE_STYLES}`;
    this.toggle.ariaLabel = 'Toggle Sidebar Details View';
    this.toggle.title = 'Toggle Sidebar Details View';

    this.sidebar.insertBefore(div, sidebarContent);
    const existingButton = this.sidebarHeader.querySelector('button');
    if (existingButton) {
      existingButton.parentNode.insertBefore(this.toggle, existingButton);
    } else {
      this.sidebarHeader.appendChild(this.toggle);
    }
    this.setUIState();
  }

  styleFrame() {
    const style = document.createElement('style');
    style.textContent = `
      .navbar,
      .alert-wrapper,
      .right-sidebar,
      .nav-sidebar,
      .issuable-gutter-toggle,
      .issue-sticky-header {
        display: none !important;
      }

      .content-wrapper {
        margin-top: 0 !important;
        padding-right: 0 !important;
      }

      .layout-page.page-gutter {
        padding-left: 0 !important;
      }
    `;
    const iframeDefaultTarget = '_top';
    const base = document.createElement('base');
    base.setAttribute('target', iframeDefaultTarget);

    try {
      const iframeDocument = this.frame.contentWindow.document;
      iframeDocument.head.appendChild(style);
      iframeDocument.head.appendChild(base);
      [...iframeDocument.querySelectorAll('a')]
        .filter(a => a.target === '_self')
        .forEach(a => a.target = iframeDefaultTarget);
    } catch (err) {
      console.warn('GitLab Details Sidebar was unable to style the child frame.');
      console.debug(err);
    }

    this.frame.classList.remove(CUSTOM_CLASSES.IFRAME_LOADING);
  }

  toggleExpansion() {
    this.expanded = !this.expanded;
    localStorage.setItem(LOCAL_STORAGE_KEY, `${this.expanded}`);
    this.setUIState();
  }

  setUIState() {
    if (this.expanded) {
      this.toggle.classList.remove(CUSTOM_CLASSES.TOGGLE_COLLAPSED);
      document.body.classList.remove(CUSTOM_CLASSES.COLLAPSED_STATE);
    } else {
      this.toggle.classList.add(CUSTOM_CLASSES.TOGGLE_COLLAPSED);
      document.body.classList.add(CUSTOM_CLASSES.COLLAPSED_STATE);
    }
  }

  watchForNewCards() {
    this.observer = new MutationObserver(this.onBoardMutation);
    this.observer.observe(this.element, {
      attributes: false,
      childList: true,
      subtree: true,
    });
  }

  onBoardMutation(mutationList) {
    mutationList
      .filter((mutation) => {
        if (mutation.type !== 'childList' || mutation.addedNodes.length !== 1) {
          return false;
        }
        const newEl = mutation.addedNodes[0]
        return newEl.classList &&
          newEl.classList.contains(CLASSES.CARD) &&
          !newEl.classList.contains(CLASSES.CARD_GHOST) &&
          newEl.getAttribute('draggable') !== 'false';
      })
      .forEach((mutation) => {
        const card = mutation.addedNodes[0];
        const number = this.getCardNumber(card);
        if (this.cardList[number]) {
          // Existing card, just a drag and drop event
          return;
        }
        this.cardList[number] = true;
        const highestNumber = Math.max(...Object.keys(this.cardList));
        if (number < highestNumber) {
          // New tickets will always have the highest number, this must be the loading in of an existing ticket
          return;
        }
        const link = this.getCardLink(card);
        if (!link) {
          return;
        }
        // When the board is loaded, suddenly there are loads of new cards added
        this.setIssueDebounced(link);
      });
  }

  destroy() {
    this.unbindEvents();
    this.element = null;
    this.sidebar = null;
    this.sidebarHeader = null;
  }
}

module.exports = {
  BoardEnhancement,
  CLASSES,
  CUSTOM_CLASSES,
  LOCAL_STORAGE_KEY,
  NAMESPACE,
  SELECTORS,
};
