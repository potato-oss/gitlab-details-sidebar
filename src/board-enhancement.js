import { BoardEnhancement, SELECTORS } from './_board-enhancement.class';
import { BoardEnhancementLegacy } from './_board-enhancement.legacy';

const board = document.querySelector(SELECTORS.BOARD);
const isModernGitlab = document.querySelector('.vue-portal-target');
if (isModernGitlab) {
    // eslint-disable-next-line no-unused-vars
    const boardEnhancement = new BoardEnhancement(board);
} else {
    // eslint-disable-next-line no-unused-vars
    const boardEnhancement = new BoardEnhancementLegacy(board);
}
